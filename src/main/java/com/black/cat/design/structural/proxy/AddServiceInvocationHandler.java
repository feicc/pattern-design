package com.black.cat.design.structural.proxy;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @description:
 * @version: 1.0
 * @create：2023/1/3 9:50
 * @author：blackcat
 */
@Slf4j
public class AddServiceInvocationHandler implements InvocationHandler {

    private AddService addService;

    public AddServiceInvocationHandler(AddService addService) {
        this.addService = addService;
    }


    /**
     * 通过生成的动态代理类, 调用真正被代理类的执行方法
     *
     * @param proxy  动态代理生成后的代理类
     * @param method 被代理类的方法, 相当于例子中add(String obj)方法
     * @param args   调用方法入参列表, 相当于例子中add(String obj)的参数
     * @return       执行add(String obj)的返回参数
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        // result 是方法执行返回值 对应 add(String obj) 的返回值
        // 这里留下个问题, 为什么要代理 addService 而不是 proxy   会无限递归
        Object invoke = method.invoke(addService, args);
        System.out.println("  >>> 发送消息通知, 执行方法名称 ::"+ method.getName()+  "方法参数："+args[0]);
        return invoke;
    }
}
