package com.black.cat.design.structural.proxy;

/**
 * @description:
 * @version: 1.0
 * @create：2023/1/3 9:48
 * @author：blackcat
 */
public interface AddService {

    boolean add(String args);
}
