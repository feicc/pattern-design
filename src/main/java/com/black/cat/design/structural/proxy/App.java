package com.black.cat.design.structural.proxy;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Proxy;

/**
 *
 * 代理模式：为其他对象提供一种代理以控制对这个对象的访问。
 */
@Slf4j
public class App {

    public static void main(String[] args) {
        // 将 Proxy.newProxyInstance 生成的动态代理类存放到磁盘中
        // 默认生成路径 com.sun.proxy 包下
        System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");
        AddService addService = new AddServiceImpl();
        AddServiceInvocationHandler handler = new AddServiceInvocationHandler(addService);
        // 这里的类加载器为 AppClassLoader, 不明白的可以看下类加载器相关知识
        AddService addServiceProxy = (AddService) Proxy
                .newProxyInstance(addService.getClass().getClassLoader(), addService.getClass().getInterfaces(), handler);
        boolean isSuccess = addServiceProxy.add("黑猫");
        System.out.println("  >>> 动态代理执行 add 方法返回值为 :: " + isSuccess);
    }
}
