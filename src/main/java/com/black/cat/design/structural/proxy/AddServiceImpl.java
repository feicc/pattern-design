package com.black.cat.design.structural.proxy;

import lombok.extern.slf4j.Slf4j;

/**
 * @description:
 * @version: 1.0
 * @create：2023/1/3 9:49
 * @author：blackcat
 */
@Slf4j
public class AddServiceImpl implements AddService {
    @Override
    public boolean add(String args) {
        log.info("模拟新增:[{}]",args);
        return true;
    }
}
