package com.black.cat.design.behaviour.visitor;

/**
 * @description: 访问者模式：允许一个或多个操作应用到一组对象(1-n,n-n),解耦操作和对象
 *
 *      需求：把资源文件（PPT、PDF、WORD）的文本内容提取到txt中
 *
 *
 * @version: 1.0
 * @create：2023/3/7 14:14
 * @author：blackcat
 */
public class App {

    public static void main(String[] args) {

    }
}


abstract class ResourceFile{

    private String filePath;

    public ResourceFile(String filePath) {
        this.filePath = filePath;
    }

    abstract void extract2Tet();
}

class PPTFile extends  ResourceFile{

    public PPTFile(String filePath) {
        super(filePath);
    }

    @Override
    void extract2Tet() {
        System.out.println("ppt extract");
    }
}

class PDFFile extends  ResourceFile{

    public PDFFile(String filePath) {
        super(filePath);
    }

    @Override
    void extract2Tet() {
        System.out.println("pdf extract");
    }
}

class WORDFile extends  ResourceFile{

    public WORDFile(String filePath) {
        super(filePath);
    }

    @Override
    void extract2Tet() {
        System.out.println("word extract");
    }
}