package com.black.cat.design.create.factory;

public interface TVFactory {
    /**
     * 生成TV 的工厂方法接口定义
     * @return
     */
    public TV produceTV();
}