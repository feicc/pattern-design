package com.black.cat.design.create.singleton;

/**
 * @description:静态内部类-线程安全
 * @version: 1.0
 * @create：2023/1/5 14:10
 * @author：blackcat
 */
public class Singleton04 {

    private Singleton04(){}

    public static Singleton04 getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        private static final Singleton04 INSTANCE = new Singleton04();
    }
}
