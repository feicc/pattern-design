package com.black.cat.design.create.singleton;

/**
 * @description:双重检查锁
 * @version: 1.0
 * @create：2023/1/6 13:05
 * @author：blackcat
 */
public class Singleton05 {

    private volatile static Singleton05 singleton;

    private Singleton05() {

    }

    public static Singleton05 getInstance() {
        if (singleton == null) {
            synchronized (Singleton05.class) {
                if (singleton == null) {
                    singleton = new Singleton05();
                }
            }
        }
        return singleton;
    }
}
