package com.black.cat.design.create.simplefactory;


/**
 * 简单工厂模式：提供了专门的工厂类用于创建对象，将对象的创建和对象的使用分离开
 * 优点：
 * 1、对象的创建和使用分离
 * 2、客户端不需要知道创建的具体产品类名，只需要知道创建的参数即可
 * 3、通过引入配置文件，在不修改客户端代码的情况下更换和新增产品，提高系统灵活性
 * 缺点：
 * 1、由于工厂类集中了所有的创建逻辑，职责过重，一旦不能正常工作，影响系统
 * 2、会增加类的个数，增加系统的复杂度和理解难度
 * 3、系统拓展困难，不利于系统的拓展和维护   不符合开闭原则
 * 4、使用了静态方法，无法继承
 * <p>
 * 使用场景：用于创建的对象少，客户端不关心对象如何创建，只需知道传入的参数
 */
public class App {

    public static void main(String args[]) {
        try {
            TV tv;
            String brandName = XMLUtilTV.getBrandName();
            tv = TVFactory.produceTV(brandName);
            tv.play();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
