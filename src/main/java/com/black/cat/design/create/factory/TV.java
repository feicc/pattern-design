package com.black.cat.design.create.factory;

public interface TV {
    public void play();
}