package com.black.cat.design.create.singleton;

/**
 * @description:单例模式-懒汉模式(线程安全)
 *               加锁占用资源，一般不推荐
 *
 * @version: 1.0
 * @create：2023/1/4 16:21
 * @author：blackcat
 */
public class Singleton02 {

    private static Singleton02 singleton;

    private Singleton02() {

    }

    public synchronized static Singleton02 getInstance() {
        if (singleton == null) {
            singleton = new Singleton02();
        }
        return singleton;
    }
}
