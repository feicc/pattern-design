package com.black.cat.design.create.singleton;

/**
 * @description:单例模式-饿汉模式(线程安全)
 *               可能导致内存浪费
 * @version: 1.0
 * @create：2023/1/4 16:24
 * @author：blackcat
 */
public class Singleton03 {

    /**
     * 静态变量创建实例
     */
    private static Singleton03 instance = new Singleton03();

    /**
     * 构造函数私有化
     */
    private Singleton03() {
    }

    /**
     * 静态方法返回实例
     * @return
     */
    public static Singleton03 getInstance() {
        return instance;
    }
}
