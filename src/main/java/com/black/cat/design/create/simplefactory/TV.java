package com.black.cat.design.create.simplefactory;

public interface TV
{
	public void play();
}