package com.black.cat.design.create.singleton;

/**
 * @description:枚举
 * @version: 1.0
 * @create：2023/1/6 13:19
 * @author：blackcat
 */
public enum Singleton07 {

    INSTANCE;

    public void test(){
        System.out.println("hi~");
    }
}
