package com.black.cat.design.create.singleton;

/**
 * 单例模式：保证一个类仅有一个实例，并提供一个访问它的全局访问点。
 * 单例模式-懒汉模式(线程不安全)
 * 1.构造函数私有化(单例模式有一个特点就是不允许外部直接创建)
 * 2.静态方法返回实例
 * 3.属性需要静态化
 *
 *
 *
 */
public class Singleton {

    private static Singleton singleton;

    private Singleton() {

    }

    public static Singleton getInstance() {
        if (singleton == null) {
            singleton = new Singleton();
        }
        return singleton;
    }

    public static void main(String[] args) {
        //懒汉式  线程不安全方式，适合单线程使用
        //模拟多线程下
        // 结果并不唯一，可能会出现相同，也有可能不同，多测几次，就能发现是线程不安全的。
        Thread thread1 = new Thread(()->{
            Singleton instance = Singleton.getInstance();
            System.out.println(instance.hashCode());
        });
        Thread thread2 = new Thread(()->{
            Singleton instance = Singleton.getInstance();
            System.out.println(instance.hashCode());
        });
        thread1.start();
        thread2.start();

    }
}
