package com.black.cat.algorithm.queue;

/**
 * @description:
 * https://juejin.cn/post/6999090895618310158
 *
 *    队列：FIFO
 *
 *   单端队列接口
 *
 *  Java泛型中的标记符含义：
 *  E - Element （元素,在集合中使用）
 *  T - Type （Java类）
 *  K - Key （键）
 *  V - Value （值）
 *  N - Number（数值类型）
 *  ? - 表示不确定的Java类型
 * @version: 1.0
 * @create：2023/3/21 13:29
 * @author：blackcat
 */
public interface Queue<E> {

    /**
     * 增加一个元索如果队列已满，则抛出一个IIIegaISlabEepeplian异常
     * @param e
     * @return
     */
    boolean add(E e);

    /**
     * 添加一个元素并返回true
     * 如果队列已满，则返回false
     * @param e
     * @return
     */
    boolean offer(E e);

    /**
     *移除并返问队列头部的元素
     * 如果队列为空，则返回null
     * @return
     */
    E poll();

    /**
     *
     * 返回队列头部的元素
     * 如果队列为空，则返回null
     * @return
     */
    E peek();
}
