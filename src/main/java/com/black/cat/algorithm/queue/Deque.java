package com.black.cat.algorithm.queue;



/**
 * @description:双端队列接口
 * @version: 1.0
 * @create：2023/3/21 13:32
 * @author：blackcat
 */
public interface Deque<E> extends Queue<E> {
}
