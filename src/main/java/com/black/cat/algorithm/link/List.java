package com.black.cat.algorithm.link;

/**
 * @description:
 * @version: 1.0
 * @create：2023/2/1 14:35
 * @author：blackcat
 */
public interface List<E> {

    /**
     * 插入
     * @param e
     * @return
     */
    boolean add(E e);

    /**
     * 插入首节点
     * @param e
     * @return
     */
    boolean addFirst(E e);

    /**
     * 插入尾节点
     * @param e
     * @return
     */
    boolean addLast(E e);

    /**
     * 删除元素
     * @param o
     * @return
     */
    boolean remove(Object o);

    /**
     * 获取元素
     * @param index
     * @return
     */
    E get(int index);
}
