package com.black.cat.algorithm.tree;

import lombok.Builder;
import lombok.Data;

/**
 * @description:
 * @version: 1.0
 * @create：2023/3/13 17:03
 * @author：blackcat
 */
@Data
@Builder
public class Node {

    public Integer value;

    public Node parent;

    public Node left;

    public Node right;
}
