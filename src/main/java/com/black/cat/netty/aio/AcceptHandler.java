package com.black.cat.netty.aio;

import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

/**
 * @description:  https://juejin.cn/post/7130952602350534693
 * @version: 1.0
 * @create：2023/2/10 9:16
 * @author：blackcat
 */
public class AcceptHandler implements CompletionHandler<AsynchronousSocketChannel, Object> {

    private final AsynchronousServerSocketChannel ssc;

    public AcceptHandler(AsynchronousServerSocketChannel ssc) {
        this.ssc = ssc;
    }

    @Override
    public void completed(AsynchronousSocketChannel result, Object attachment) {

    }

    @Override
    public void failed(Throwable exc, Object attachment) {

    }
}
