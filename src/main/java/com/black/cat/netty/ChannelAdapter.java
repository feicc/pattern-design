package com.black.cat.netty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.nio.charset.Charset;

/**
 * @description:
 * @version: 1.0
 * @create：2023/2/6 11:01
 * @author：blackcat
 */
public abstract class ChannelAdapter extends Thread {

    private Socket socket;

    private Charset charset;


    /**
     * Socket+ Charset 的通道处理
     */
    private ChannelHandler channelHandler;


    public ChannelAdapter(Socket socket, Charset charset) {
        this.socket = socket;
        this.charset = charset;
        //这块可以加一些多长时间就放弃等待。或者休眠
        while (!socket.isConnected()) {
            break;
        }
        //Socket+ Charset 组合 创建处理通道
        channelHandler = new ChannelHandler(this.socket, charset);
        channelActive(channelHandler);
    }

    @Override
    public void run(){
        try {
            BufferedReader input = new BufferedReader(new InputStreamReader(this.socket.getInputStream(), charset));
            String str = null;
            while ((str = input.readLine()) != null) {
                channelRead(channelHandler, str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    // 链接通知抽象类
    public abstract void channelActive(ChannelHandler ctx);

    // 读取消息抽象类
    public abstract void channelRead(ChannelHandler ctx, Object msg);



}
